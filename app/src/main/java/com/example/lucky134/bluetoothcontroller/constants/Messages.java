package com.example.lucky134.bluetoothcontroller.constants;

/**
 * Created by shpp-admin on 2/19/2017.
 */

public enum Messages {
    DISCOVERED_DEVICE,
    DISCOVER_FINISHED,
    PAIRED,
    PAIR_FAILED,
    PAIRING
}

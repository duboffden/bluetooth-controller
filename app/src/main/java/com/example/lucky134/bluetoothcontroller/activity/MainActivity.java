package com.example.lucky134.bluetoothcontroller.activity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.lucky134.bluetoothcontroller.MessageEvent;
import com.example.lucky134.bluetoothcontroller.R;
import com.example.lucky134.bluetoothcontroller.constants.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.example.lucky134.bluetoothcontroller.constants.BluetoothStates.CONNECTED;
import static com.example.lucky134.bluetoothcontroller.constants.BluetoothStates.CONNECTING;
import static com.example.lucky134.bluetoothcontroller.constants.BluetoothStates.DISCOVERING;
import static com.example.lucky134.bluetoothcontroller.constants.BluetoothStates.NOT_CONNECTED;
import static com.example.lucky134.bluetoothcontroller.constants.BluetoothStates.PAIRING;
import static com.example.lucky134.bluetoothcontroller.constants.Constants.REQUEST_ENABLE_BT;

public class MainActivity extends AppCompatActivity implements Spinner.OnItemSelectedListener {
    // TODO: Refactor this
    public static final String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.s_connected_devices)
    Spinner sDevicesSpinner;
    @BindView(R.id.ib_refresh_devices)
    ImageButton ibRefreshDevices;
    @BindView(R.id.pb_refresh_devices)
    ProgressBar pbRefreshDevices;

    private ArrayList<BluetoothDevice> mPairedDevices = new ArrayList<>();
    private ArrayAdapter<String> mDiscoveredDevicesSpinnerAdapter;
    private ArrayList<String> mDevicesNames = new ArrayList<>();
    private OutputStream mBluetoothOutputStream;
    private BluetoothSocket mSocketConnection;
    private BluetoothAdapter mBluetoothAdapter;
    private AsyncTask mConnectingTask;

    private int nBluetoothState = NOT_CONNECTED;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mBluetoothAdapter = ((BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();

        if (mBluetoothAdapter == null) { // if device doesnt support bluetooth we just finish activity
            Toast.makeText(getApplicationContext(), R.string.no_bluetooth, Toast.LENGTH_SHORT).show();
            Log.e(TAG, "Device not supporting bluetooth; MainActivity.java:41");
            finish();
        }

        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) // get to know bluetooth enabled or no
            requestBluetooth();
        else
            setup();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode != RESULT_OK) {
            Log.e(TAG, "User doesn't wants to enable bluetooth; MainActivity.java:116");
            finish();
        }

        setup();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position == Constants.NOTHING_SELECTED) {
            if (isBluetoothBusy())
                return;

            if (isConnected())
                disconnect();

            nBluetoothState = NOT_CONNECTED;
            setSubtitle(getString(R.string.device_not_connected));

            return;
        }

        if (isBluetoothBusy()) {
            makeSnackbar(getString(R.string.bluetooth_busy));
            return;
        }

        BluetoothDevice mDeviceToConnect = mPairedDevices.get(position);
        Log.d("TAG", "mDeviceToConnect " + mDeviceToConnect);

        if (mDeviceToConnect == null)
            return;
        Log.d("TAG", "mDeviceToConnect getBondState " + mDeviceToConnect.getBondState());

        if (mDeviceToConnect.getBondState() == BluetoothDevice.BOND_NONE)
            pairDevice(mDeviceToConnect);
        else
            connectToBluetoothDevice(mDeviceToConnect);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSocketConnection != null && mSocketConnection.isConnected() && mBluetoothOutputStream != null)
            try {
                mBluetoothOutputStream.close();
                mSocketConnection.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    private void setup() {
        setupActionBar();
        setupPairedDevicesSpinner();
        initListeners();
        addPairedDevices();
    }

    public void setupActionBar() { // configures actionbar
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        if (mSocketConnection == null
                || !mSocketConnection.isConnected())
            setSubtitle(getString(R.string.device_not_connected));
        else if (mSocketConnection != null
                && mSocketConnection.isConnected())
            setSubtitle(mSocketConnection.getRemoteDevice().getName());
    }


    private void setupPairedDevicesSpinner() {
        mDiscoveredDevicesSpinnerAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, mDevicesNames);

        mDiscoveredDevicesSpinnerAdapter.setDropDownViewResource(R.layout.layout_spinner_item);

        sDevicesSpinner.setAdapter(mDiscoveredDevicesSpinnerAdapter);

        sDevicesSpinner.setOnItemSelectedListener(this);
    }

    private void initListeners() {
        ibRefreshDevices.setOnClickListener((v) -> discoverDevices());
    }

    private void discoverDevices() {

        nBluetoothState = DISCOVERING;

        showProgressBar();
        setSubtitle(getString(R.string.discovering_devices));

        addPairedDevices();

        mBluetoothAdapter.startDiscovery();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent messageEvent) {
        switch (messageEvent.getMessage()) {
            case DISCOVERED_DEVICE:
                addDiscoveredDevice((BluetoothDevice) messageEvent.getDelivery());
                break;

            case DISCOVER_FINISHED:
                showDiscoverFinished();
                break;

            case PAIRED:
                showPaired((BluetoothDevice) messageEvent.getDelivery());
                break;

            case PAIR_FAILED:
                showPairFailed();
                break;
            case PAIRING:
                showPairing((BluetoothDevice) messageEvent.getDelivery());
        }
    }

    private void addDiscoveredDevice(BluetoothDevice discoveredDevice) {
        if (discoveredDevice == null)
            return;

        if (discoveredDevice.getName() == null)
            return;

        mPairedDevices.add(discoveredDevice);
        mDevicesNames.add(discoveredDevice.getName());
        mDiscoveredDevicesSpinnerAdapter.notifyDataSetChanged();

    }

    private void showPairing(BluetoothDevice bluetoothDevice) {
        nBluetoothState = PAIRING;

        setSubtitle(String.format(getString(R.string.pairing_to), bluetoothDevice.getName()));
        showProgressBar();
    }

    private void showDiscoverFinished() {
        nBluetoothState = NOT_CONNECTED;
        setSubtitle(getString(R.string.device_not_connected));
        hideProgeressBar();
    }

    private void showPaired(BluetoothDevice bluetoothDevice) {
        hideProgeressBar();

        if (bluetoothDevice == null)
            return;


        if (bluetoothDevice.getName() == null)
            return;

        nBluetoothState = NOT_CONNECTED;

        makeSnackbar(String.format(getString(R.string.paired_to), bluetoothDevice.getName()));

        connectToBluetoothDevice(bluetoothDevice);
    }

    private void showPairFailed() {
        nBluetoothState = NOT_CONNECTED;
        hideProgeressBar();
        setSubtitle(getString(R.string.device_not_connected));
        makeSnackbar(getString(R.string.pairing_failed));
    }

    private void setSubtitle(String subtitle) {
        if (getSupportActionBar() != null)
            getSupportActionBar().setSubtitle(subtitle);
    }

    private void requestBluetooth() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
    }

    public void sendMessage(byte message) { // use this function for sending command to bluetooth device

        if (mSocketConnection == null
                || !mSocketConnection.isConnected()
                || mBluetoothOutputStream == null) {
            makeSnackbar(getString(R.string.device_not_connected));
            return;
        }

        try {
            mBluetoothOutputStream.write(message);
        } catch (IOException e) {
            e.printStackTrace();

            makeSnackbar(getString(R.string.cant_send_command));
        }
    }

    private void pairDevice(BluetoothDevice device) {

        Log.d("TAG", "pair device");

        if (isBluetoothBusy()) {
            makeSnackbar(getString(R.string.bluetooth_busy));
            return;
        }

        try {
            nBluetoothState = PAIRING;
            Method method = device.getClass().getMethod("createBond", (Class[]) null);
            method.invoke(device, (Object[]) null);
        } catch (Exception e) {
            nBluetoothState = NOT_CONNECTED;
            e.printStackTrace();
        }
    }

    private void showProgressBar() {
        ibRefreshDevices.setVisibility(GONE);
        pbRefreshDevices.setVisibility(VISIBLE);
    }

    private void hideProgeressBar() {
        ibRefreshDevices.setVisibility(VISIBLE);
        pbRefreshDevices.setVisibility(GONE);
    }

    private void connectToBluetoothDevice(BluetoothDevice bluetoothDevice) {
        if (isBluetoothBusy()) {
            makeSnackbar(getString(R.string.bluetooth_busy));
            return;
        }

        if (isConnected())
            disconnect();

        startConnectionAsyncTask(bluetoothDevice);
    }

    private void startConnectionAsyncTask(BluetoothDevice bluetoothDevice) {
        if (bluetoothDevice == null)
            return;

        if (bluetoothDevice.getName() == null)
            return;

        setSubtitle(String.format(getString(R.string.connecting_to), bluetoothDevice.getName()));
        showProgressBar();
        nBluetoothState = CONNECTING;

        mConnectingTask = new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                return tryToConnect(bluetoothDevice);
            }

            @Override
            protected void onPostExecute(Boolean isConnected) {
                if (isConnected) {
                    nBluetoothState = CONNECTED;
                    setSubtitle(String.format(getString(R.string.connected_to), bluetoothDevice.getName()));
                } else {
                    nBluetoothState = NOT_CONNECTED;
                    setSubtitle(getString(R.string.device_not_connected));
                    makeSnackbar(getString(R.string.connection_error));
                }

                hideProgeressBar();
                mConnectingTask = null;
            }
        }.execute();
    }

    private boolean tryToConnect(BluetoothDevice device) {
        if (device == null)
            return false;

        try {
            mSocketConnection = device.createRfcommSocketToServiceRecord(Constants.MY_UUID_SECURE);
            mSocketConnection.connect();
            mBluetoothOutputStream = mSocketConnection.getOutputStream();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    private void makeSnackbar(String text) {
        if (text == null)
            return;

        if (text.isEmpty())
            return;

        Snackbar.make(findViewById(R.id.relativeLayout), text, Snackbar.LENGTH_SHORT).show();
    }

    private boolean isConnected() {
        if (mBluetoothOutputStream == null)
            return false;

        //noinspection SimplifiableIfStatement
        if (mSocketConnection == null)
            return false;

        return mSocketConnection.isConnected();
    }

    private void disconnect() {
        try {
            if (mSocketConnection != null)
                mSocketConnection.close();

            if (mBluetoothOutputStream != null)
                mBluetoothOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addPairedDevices() {
        mPairedDevices.clear();
        mDevicesNames.clear();
        Set<BluetoothDevice> mPairedSet = mBluetoothAdapter.getBondedDevices();

        mDevicesNames.add(getString(R.string.nothing_selected));
        mPairedDevices.add(null);

        for (BluetoothDevice bt : mPairedSet) {
            if (bt == null)
                continue;

            if (bt.getName() == null)
                continue;

            if (bt.getName().isEmpty())
                continue;

            mPairedDevices.add(bt);
            mDevicesNames.add(bt.getName());
        }

        mDiscoveredDevicesSpinnerAdapter.notifyDataSetChanged();
    }

    private boolean isBluetoothBusy() {
        return nBluetoothState == CONNECTING || nBluetoothState == PAIRING || nBluetoothState == DISCOVERING;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
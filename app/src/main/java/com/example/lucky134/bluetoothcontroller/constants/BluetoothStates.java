package com.example.lucky134.bluetoothcontroller.constants;

/**
 * Created by Dubovitsky Denis on 2/19/2017.
 */

public class BluetoothStates {
    public static final int NOT_CONNECTED = 0;
    public static final int CONNECTING = 1;
    public static final int CONNECTED = 2;
    public static final int DISCOVERING = 3;
    public static final int PAIRING = 4;
}

package com.example.lucky134.bluetoothcontroller.constants;

import java.util.UUID;

/**
 * Created by Denis Dubovitsky on 2/8/2017.
 */

public class Constants {
    public static final int REQUEST_ENABLE_BT = 1;


    public static final UUID MY_UUID_SECURE =
            UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    public static final int NOTHING_SELECTED = 0;
}

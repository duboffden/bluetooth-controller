package com.example.lucky134.bluetoothcontroller.receivers;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.lucky134.bluetoothcontroller.MessageEvent;
import com.example.lucky134.bluetoothcontroller.constants.Messages;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Dubovitsky Denis on 2/19/2017.
 */

public class BluetoothReceiver extends BroadcastReceiver {

    public static final String TAG = BluetoothReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.d(TAG, "BluetoothReceiver onReceive " + action);

        switch (action) {
            case BluetoothDevice.ACTION_FOUND:
                EventBus.getDefault().post(new MessageEvent(Messages.DISCOVERED_DEVICE,
                        intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)));
                break;

            case BluetoothAdapter.ACTION_DISCOVERY_FINISHED:
                EventBus.getDefault().post(new MessageEvent(Messages.DISCOVER_FINISHED));
                break;

            case BluetoothDevice.ACTION_BOND_STATE_CHANGED:
                switch (intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR)) {

                    case BluetoothDevice.BOND_BONDING:
                        EventBus.getDefault().post(new MessageEvent(Messages.PAIRING,
                                intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)));
                        break;

                    case BluetoothDevice.BOND_BONDED:
                        EventBus.getDefault().post(new MessageEvent(Messages.PAIRED,
                                intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)));
                        break;

                    case BluetoothDevice.BOND_NONE:
                        EventBus.getDefault().post(new MessageEvent(Messages.PAIR_FAILED));
                        break;

                    case BluetoothDevice.ERROR:
                        EventBus.getDefault().post(new MessageEvent(Messages.PAIR_FAILED));
                        break;
                }
                Log.d(TAG, "Bond state = " + intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR));
                break;
        }
    }
}

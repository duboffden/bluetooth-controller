package com.example.lucky134.bluetoothcontroller;

import com.example.lucky134.bluetoothcontroller.constants.Messages;

/**
 * Created by Denis Dubovitsky on 2/19/2017.
 */

public class MessageEvent {
    private Messages message;
    private Object delivery;

    public MessageEvent(Messages message){
        this.message = message;
    }

    public MessageEvent(Messages message, Object delivery){
        this.message = message;
        this.delivery = delivery;
    }

    public Messages getMessage() {
        return message;
    }

    public Object getDelivery() {
        return delivery;
    }
}
